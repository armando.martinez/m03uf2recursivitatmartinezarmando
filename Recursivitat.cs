﻿/*
* AUTHOR: Armando Martínez
* DATE: 2023 - 01 - 18
* DESCRIPTION: Conjunt d'exercicis de parametrització
*/

using System;

namespace M03UF2RecursivitatMartinezArmando
{
    class Recursivitat
    {
        static void Main(string[] args)
        {
            Start();
        }
        // Inici del programa
        static void Start()
        {
            int selectedOption;
            do
            {
                Console.WriteLine("EXERCICIS DE PARAMETRITZACIÓ");
                selectedOption = Menu();
                if (selectedOption != 0) { Console.Clear(); }
                switch (selectedOption)
                {
                    case 0:
                        Console.WriteLine("\n[EL PROGRAMA ES TANCARÀ . . .]");
                        break;
                    case 1:
                        Factorial();
                        break;
                    case 2:
                        DoubleFactorial();
                        break;
                    case 3:
                        DigitNumbers();
                        break;
                    case 4:
                        GrowingNumbers();
                        break;
                    case 5:
                        ShrinkingDigits();
                        break;
                    case 6:
                        AsteriskSequence();
                        break;
                    case 7:
                        PerfectPrimaries();
                        break;
                    case 8:
                        HanoiTowers();
                        break;
                }
                Console.WriteLine("\n[ENTER PER CONTINUAR]");
                Console.ReadLine();
                Console.Clear();
            } while (selectedOption != 0);
        }
        // Mostra un menú que permet escollir entre varies opcions i retorna la decisió de l'usuari
        static int Menu()
        {
            string[] options = { " 0 - Exit", " 1 - Factorial", " 2 - Doble factorial", " 3 - Nombre de dígits",
                            " 4 - Nombres creixents", " 5 - Reducció de dígits", " 6 - Seqüència d'asteriscos",
                            " 7 - Primers perfectes", " 8 - Torres de Hanoi"};
            foreach (string option in options) { Console.WriteLine(option); }
            int selectedOption = ParseIntInRange("\nSELECCIONA UNA OPCIÓ: ", 0, 8);
            return selectedOption;
        }
        /* -- -- -- -- -- -- -- -- --  -- -- */
        // Recull una dada que obligatoriament ha de ser un integer dins d'un cert rang
        static int ParseIntInRange(string prompt, int min, int max)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum > max || parsedNum < min) { Console.WriteLine($"El número ha d'estar entre {min} i {max}!"); }
            } while (!isParsed || parsedNum > max || parsedNum < min);
            return parsedNum;
        }
        // Recull una dada que obligatoriament ha de ser un integer positiu
        static int ParseIntPositive(string prompt)
        {
            bool isParsed;
            int parsedNum;
            do
            {
                Console.Write(prompt);
                string userNum = Console.ReadLine();
                isParsed = int.TryParse(userNum, out parsedNum);
                if (!isParsed) { Console.WriteLine("El format es incorrecte!"); }
                else if (parsedNum < 0) { Console.WriteLine($"El número ha de ser positiu!"); }
            } while (!isParsed || parsedNum < 0);
            return parsedNum;
        }
        // Visualitza un array
        static void VisualizeArray(int[] array)
        {
            int n = array.Length;
            Console.Write("[ ");
            for (int i = 0; i < n - 1; i++)
            {
                Console.Write($"{array[i]}, ");
            }
            Console.WriteLine($"{array[n - 1]} ]");
        }

        /* -- -- -- -- FACTORIAL -- -- -- -- */
        static int DoFactorial(int n)
        {
            if (n <= 1) { return 1; }
            return n * DoFactorial(n-1);
        }
        static void Factorial()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine($"\nEl factorial de {n} es {DoFactorial(n)}.");
        }
        /* -- -- -- DOBLE FACTORIAL -- -- -- */
        static int DoDoubleFactorial(int n)
        {
            if (n <= 1) { return 1; }
            return n * DoDoubleFactorial(n-2);
        }
        static void DoubleFactorial() 
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine($"\nEl factorial de {n} es {DoDoubleFactorial(n)}.");
        }
        /* -- -- -- NOMBRE DE DÍGIT -- -- -- */
        static int CountDigits(int n)
        {
            int digits = 0;
            if (n > 0)
            {
                digits = CountDigits(n / 10);
                digits++;
            }
            return digits;
        }
        static void DigitNumbers()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine($"\nAquest número té {CountDigits(n)} digits.");
        }
        /* -- -- - NOMBRES CREIXENTS - -- -- */
        static bool IsNumberGrowing(int n)
        {
            bool isGrowing;
            if (n > 9)
            {
                isGrowing = IsNumberGrowing(n / 10);
                if (isGrowing && (n % 10) == (n / 10 % 10) + 1)
                {
                    return true;
                }
                else { return false; }
            }
            return true;
        }
        static void GrowingNumbers()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine(IsNumberGrowing(n));
        }
        /* -- -- - REDUCCIÓ DE DIGIT - -- -- */
        static int ShrunkDigit(int n)
        {
            if (n > 9)
            {
                int sum = 0;
                do
                {
                    sum += n % 10;
                    n /= 10;
                } while (n > 0);
                if (sum > 9)
                {
                    return ShrunkDigit(sum);
                }
                return sum;
            }
            return n;
        }
        static void ShrinkingDigits()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine(ShrunkDigit(n));
        }
        /* -- -- SEQÜÈNCIA D'ASTERISCS -- -- */
        static int PrintAsteriskSequence(int n)
        {
            if (n > 0)
            {
                PrintAsteriskSequence(n - 1);
                Console.WriteLine();
                for (int i = 0; i < n; i++) { Console.Write("*"); }
                return PrintAsteriskSequence(n - 1);
            }
            return 1;
        }
        static void AsteriskSequence() 
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            PrintAsteriskSequence(n);
        }
        /* -- --- PRIMARIS  PERFECTES --- -- */
        static bool IsNumPrime(int possiblePrime, int num)
        {
            if (num > 1)
            {
                if (possiblePrime % num != 0)
                {
                    return IsNumPrime(possiblePrime, num - 1);
                }
                return false;
            }            
            return true;
        }

        static void PerfectPrimaries()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            Console.WriteLine(IsNumPrime(n, n - 1));
        }
        /* -- -- -- TORRES DE HANOI -- -- -- */
        
        static int GetSmallest(int[] array)
        {
            int smallest = array[0];
            int smallestIndex = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (smallest == 0 && array[i] != 0) { smallest = array[i]; }
                if (array[i] < smallest && array[i] != 0)
                {
                    smallest = array[i];
                    smallestIndex = i;
                }
            }
            return smallestIndex;
        }
        static void MoveHanoi(int[] arraySource, int[] arrayTarget, int[] a, int[] b, int[] c)
        {
            int nextA = GetSmallest(arraySource);
            int nextB = GetSmallest(arrayTarget);
            if (arrayTarget[nextB] != 0)
            {
                arrayTarget[nextB + 1] = arraySource[nextA];
            } else
            {
                arrayTarget[nextB] = arraySource[nextA];
            }
            arraySource[nextA] = 0;
            if (arraySource == a && arrayTarget == b) { Console.WriteLine("A ==> B"); }
            else if (arraySource == a && arrayTarget == c) { Console.WriteLine("A ==> C"); }
            else if (arraySource == b && arrayTarget == a) { Console.WriteLine("B ==> A"); }
            else if (arraySource == b && arrayTarget == c) { Console.WriteLine("B ==> C"); }
            else if (arraySource == c && arrayTarget == a) { Console.WriteLine("C ==> A"); }
            else if (arraySource == c && arrayTarget == b) { Console.WriteLine("C ==> B"); }
            VisualizeHanoi(a, b, c);
        }
        static void MoveTower(int[] arraySource, int[] arrayTarget, int[] arrayAux, int[] a, int[] b, int[] c, int toMove)
        {
            // este código fue escrito con inspiración divina, después de horas
            // de jugar manualmente con las torres para figurarme el algoritmo
            if (toMove == 1) 
            {
                MoveHanoi(arraySource, arrayTarget, a, b, c);
            } else if (toMove == 2)
            {
                MoveHanoi(arraySource, arrayAux, a, b, c);
                MoveHanoi(arraySource, arrayTarget, a, b, c);
                MoveHanoi(arrayAux, arrayTarget, a, b, c);
            } else {
                MoveTower(arraySource, arrayAux, arrayTarget, a, b, c, toMove - 1);
                MoveHanoi(arraySource, arrayTarget, a, b, c);
                MoveTower(arrayAux, arrayTarget, arraySource, a, b, c, toMove - 1);
            }
        }
        static void VisualizeHanoi(int[] a, int[] b, int[] c)
        {
            Console.WriteLine();
            VisualizeArray(a);
            VisualizeArray(b);
            VisualizeArray(c);
            Console.WriteLine();
        }
        static void HanoiTowers()
        {
            int n = ParseIntPositive("Introdueix un número positiu enter: ");
            int[] towerA = new int[n];
            int[] towerB = new int[n];
            int[] towerC = new int[n];
            for (int i = 0; i < n; i++)
            {
                towerA[i] = n - i;
            }
            VisualizeHanoi(towerA, towerB, towerC);
            MoveTower(towerA, towerC, towerB, towerA, towerB, towerC, n);
        }
    }
}
